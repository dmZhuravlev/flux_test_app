import React, {PropTypes} from 'react';

export default class Table extends React.Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.checkIsFavorite = this.checkIsFavorite.bind(this);
  }

  handleChange(event) {
    const id = event.target.getAttribute('data-attr');
    const checked = event.target.checked;

    this.props.addFavorite(id, checked);
  }

  checkIsFavorite(isFavorite) {
      if(!isFavorite && this.props.showFavorite){
          return true;
      }
      return false;
  }

  render() {
    let rows = [];

    this.props.data.forEach((person) => {
        rows.push(<tr key={ person.id }
                    className={ (this.checkIsFavorite(person.isFavorite) ? 'hidden-item' : '')}>
            <td className="col">{ person.firstname }</td>
            <td className="col">{ person.lastname }</td>
            <td className="col-favorite">
                <input 
                type="checkbox"
                data-attr={person.id}

                onChange={ this.handleChange }
                checked={ person.isFavorite } />
            </td>
        </tr>)
    });

    return (
        <table>
            <thead>
                <tr>
                    <th>First_name</th>
                    <th>Last_name</th>
                    <th>Add to favorite</th>
                </tr>
            </thead>
            <tbody>
                { rows }
            </tbody>
        </table>
      );
  }
}