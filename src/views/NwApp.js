import React, {PropTypes} from 'react';
import Search from './Search';
import Table from './Table';

export default class App extends React.Component {
  constructor(props) {
    super(props);


    this.doSearch = this.doSearch.bind(this);
    this.filterPropsData = this.filterPropsData.bind(this);
    this.setSearchFor = this.setSearchFor.bind(this);
    this.addToFavorite = this.addToFavorite.bind(this);
    this.handleShowFavorite = this.handleShowFavorite.bind(this);
  }

  doSearch(queryText) {
    this.queryResult = this.filterPropsData(queryText);
    this.props.onDoSearch(queryText);
  }

  setSearchFor(param) {
    this.props.onSetSearchFor(param);
  }

  filterPropsData(queryText) {
    return this.props.search.filteredData.filter((person) => {
      return (person[this.props.search.searchFor].toLowerCase().indexOf(queryText.toLowerCase()) !== -1)
    })
  }

  dataForRender() {
    if (!this.props.search.query) {
      return this.props.search.filteredData;
    }

    return this.filterPropsData(this.props.search.query)
  }

  addToFavorite(id, checked) {
    const list = this.props.search.filteredData.map((person) => {
      (person.id === id) && (person.isFavorite = checked)
      
      return person;
    });

    this.props.onAddToFavorite(list);
  }

  handleShowFavorite(event) {
    const checked = event.target.checked;

    this.props.onShowOnlyFavorite(checked);
  }

  render() {
    return (<div className="search-block">
              <h2>test search app</h2>

              <Search 
                query={this.props.search.query}
                doSearch={this.doSearch}
                searchFor={this.props.search.searchFor}
                setSearchFor={this.setSearchFor} />

              <Table
                showFavorite={this.props.search.showOnlyFavorite}
                addFavorite={this.addToFavorite}
                data={this.dataForRender()}/>

              <input 
                type="checkbox"
                onChange={ this.handleShowFavorite }
                checked={ this.props.showOnlyFavorite }/>

              <span>Show only favorite</span>

          </div>);
  }
}