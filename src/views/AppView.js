'use strict';

import React from 'react';
import NwApp from './NwApp';


function AppView(props) {
  return (
    <div>
      
      <NwApp {...props} />
    </div>
  );
}

export default AppView;
