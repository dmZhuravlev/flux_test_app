import React, {PropTypes} from 'react';

export default class Search extends React.Component {
    constructor(props) {
        super(props);

        this.startSearch = this.startSearch.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
    }

    startSearch(evt) {
        const query = evt.target.value;

        this.props.doSearch(query);
    }

    handleOptionChange(changeEvent) {
        this.props.setSearchFor(changeEvent.target.value);
    }

    render() {
        return (
            <div>
                <input type="text"
                ref="searchInput"
                placeholder="Search Name"
                value = { this.props.query }
                onChange = { this.startSearch }/>

                <br/>

                <input 
                type="radio" name="field"
                value="firstname"
                onChange={this.handleOptionChange}
                checked={this.props.searchFor === 'firstname'} /> first name

                <input
                type="radio" name="field"
                value="lastname"
                onChange={this.handleOptionChange}
                checked={this.props.searchFor === 'lastname'} /> last name
            </div>
        );
    }
}