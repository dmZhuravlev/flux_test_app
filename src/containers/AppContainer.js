'use strict';

import AppView from '../views/AppView';
import {Container} from 'flux/utils';
import AppActions from '../data/AppActions';


import SearchStore from '../data/SearchStore';

function getStores() {
  return [
    SearchStore
  ];
}

function getState() {
  return {

    search: SearchStore.getState(),

    onDoSearch: AppActions.doSearch,
    onSetSearchFor: AppActions.setSearchFor,
    onAddToFavorite: AppActions.addToFavorite,
    onShowOnlyFavorite: AppActions.showOnlyFavorite
  };
}

export default Container.createFunctional(AppView, getStores, getState);
