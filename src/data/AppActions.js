'use strict';

import AppActionTypes from './AppActionTypes';
import AppDispatcher from './AppDispatcher';

const Actions = {

  doSearch(queryText, queryResult) {
    AppDispatcher.dispatch({
      type: AppActionTypes.DO_SEARCH,
      queryText,
      queryResult
    });
  },

  setSearchFor(param) {
    AppDispatcher.dispatch({
      type: AppActionTypes.SET_SEARCH_FOR,
      param
    });
  },

  addToFavorite(list) {
    AppDispatcher.dispatch({
      type: AppActionTypes.ADD_TO_FAVORITE,
      list
    });
  },

  showOnlyFavorite(value) {
    AppDispatcher.dispatch({
      type: AppActionTypes.SHOW_ONLY_FAVORITE,
      value
    });
  }
};

export default Actions;
