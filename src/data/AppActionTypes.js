'use strict';

const ActionTypes = {
  DO_SEARCH: 'DO_SEARCH',
  SET_SEARCH_FOR: 'SET_SEARCH_FOR',
  ADD_TO_FAVORITE: 'ADD_TO_FAVORITE',
  SHOW_ONLY_FAVORITE: 'SHOW_ONLY_FAVORITE'
};

export default ActionTypes;
