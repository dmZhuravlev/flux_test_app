'use strict';

import {ReduceStore} from 'flux/utils';
import AppActionTypes from './AppActionTypes';
import AppDispatcher from './AppDispatcher';

const DATA = [{
  firstname:'Paul Shan',
  lastname: 'lastname_1',
  isFavorite: false,
  id: '1'
},
{
  firstname:'John Doe',
  lastname: 'lastname_2',
  isFavorite: false,
  id: '2'
},
{
  firstname:'Paul Shan',
  lastname: 'lastname_3',
  isFavorite: false,
  id: '3'
},
{
  firstname:'Sachin Tendulkar',
  lastname: 'lastname_4_4_4',
  isFavorite: false,
  id: '4'
},
{
  firstname:'John Doe',
  lastname: 'lastname_5',
  isFavorite: false,
  id: '5'
}];

class SearchStore extends ReduceStore {
  constructor() {
    super(AppDispatcher);
  }

  getInitialState() {
    return {
      query: '',
      filteredData: DATA,
      searchFor: 'firstname',
      showOnlyFavorite: false
    };
  }

  reduce(state, action) {
    switch (action.type) {

        case AppActionTypes.DO_SEARCH:
            return {
                ...state,
                query: action.queryText
            };

        case AppActionTypes.SET_SEARCH_FOR:
            return {
                ...state,
                searchFor: action.param
            };

        case AppActionTypes.ADD_TO_FAVORITE:
            return {
              ...state,
              filteredData: action.list
            }

        case AppActionTypes.SHOW_ONLY_FAVORITE:
            return {
              ...state,
              showOnlyFavorite: action.value
            }

        default:
            return state;
    }
  }
}

export default new SearchStore();